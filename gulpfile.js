'use strict';

const fs = require('fs');
const gulp = require('gulp');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob')
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const pxtorem = require('postcss-pxtorem');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');
const stylelint = require('stylelint');
const reporter = require('postcss-reporter');
const path = require('path');
const printf = require('printf');
const {
    gray,
    redBright,
    yellowBright,
    blueBright,
    underline } = require('colorette');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const root = "../../../../";
let config = "jlgulp-config.json";

if (fs.existsSync(root + config)) {
    config = require(path.resolve(root, config));
} else {
    throw new Error(config + " not found in " + path.resolve(root));
}

function styles() {
    const plugins = [
        autoprefixer(),
        pxtorem({
            rootValue: 16,
            unitPrecision: 5,
            propList: [
                "*",
                "!letter-spacing",
                "!*box*",
                "!line-height",
                "!border*",
                "!background*"
            ],
            replace: true,
            mediaQuery: true
        }),
        cssnano()
    ];

    return gulp.src(config.paths.scss)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.paths.css))
        .pipe(browserSync.stream());
}

function lint() {
    const plugins = [
        stylelint({
            options: ".stylelintrc.json"
        }),
        reporter({
            clearReportedMessages: true,
            formatter: function (input) {
                let output = '';
                let length = 0;
                let source = input.source

                output += underline(path.relative(process.cwd(), source).split(path.sep).join('/')) + '\n';

                input.messages.forEach(function (i) {
                   let text = i.text.slice(0, i.text.indexOf(' ('));
                   if (text.length > length) {
                       length = text.length;
                   }
                });

                input.messages.forEach(function (i) {
                    let type;
                    let position;
                    let text;
                    let rule;

                    switch(i.type) {
                       case 'warning':
                           type = yellowBright('!  ');
                           break;
                       case 'error':
                           type = redBright('x  ');
                           break;
                       default:
                           type = blueBright('i  ');
                    }

                    position = i.line + ':' + i.column + '  ';
                    text = i.text.slice(0, i.text.indexOf(' ('));
                    rule = '  ' + gray(i.rule);

                    output += printf("%3s%8s%-*s%1s \n", type, position, text, length, rule);
                });

                return output;
            }
        })
    ];

    return gulp.src(config.paths.scss).pipe(
        postcss(plugins, {syntax: require('postcss-scss')})
    );
}

function jsMinify() {
    return gulp.src(config.paths.js)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(rename('scripts.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.paths.jsDest));
}

function imgMinify() {
    return gulp.src(config.paths.img)
        .pipe(imagemin())
        .pipe(gulp.dest(config.paths.imgDest))
}

function watch() {
    browserSync.init({
        proxy: config.url
    });

    gulp.watch(config.paths.scss, gulp.series(lint, styles));
    gulp.watch(config.paths.js).on("change", gulp.series(jsMinify, browserSync.reload));
    gulp.watch(config.paths.img).on("change", gulp.series(imgMinify, browserSync.reload));
}

exports.default = gulp.series(lint, styles, jsMinify, imgMinify, watch);
exports.build = gulp.series(lint, styles, jsMinify, imgMinify);