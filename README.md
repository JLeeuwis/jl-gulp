# Gulp
A gulp file with multiple features

## Features
- Compile scss to css
- Combine and minify javascript
- Optimize images
- Standardize syntax with the linter function

## Installation
To install the following extension run the following command:

```composer require jleeuwis/jl-gulp```

## Requirements
- Nodejs v14
- npm v6