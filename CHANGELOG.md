# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0]
### Changed
- Default location of the configuration file

## [1.0.0]
### Added
- Scss can now be compiled to css for a more efficient workflow
- Javascript can be combined and minified for optimized scripting
- Images can be minified for better online performance
- Lint task to analyze syntax and style errors in scss
- Task to proxy a site for better developing
- A config file to change the settings with ease